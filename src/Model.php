<?php
// ------------------------------------------------------------------------
// |@Author       : Jarmin <edshop@qq.com>
// |@----------------------------------------------------------------------
// |@Date         : 2023-06-04 14:52:24
// |@----------------------------------------------------------------------
// |@LastEditTime : 2024-10-03 02:53:54
// |@----------------------------------------------------------------------
// |@LastEditors  : Jarmin <jarmin@ladmin.cn>
// |@----------------------------------------------------------------------
// |@Description  : 模型基类
// |@----------------------------------------------------------------------
// |@FilePath     : Model.php
// |@----------------------------------------------------------------------
// |@Copyright (c) 2023 http://www.ladmin.cn   All rights reserved. 
// ------------------------------------------------------------------------
declare (strict_types=1);

namespace think\admin;

/**
 * 模型基类
 * Class Model
 * @package app\model
 */
class Model extends \think\Model
{
    /**
     * 获取单条数据
     * @param array $where 查询条件
     * @param mixed $fields 需要获取的字段
     * @return array 配置信息数组，如果未找到则返回空数组
     */
    public function getOne(array $where, string $fields = '*')
    {
        return $this->where($where)->field($fields)->find();
    }
 
    /**
     * 统计符合条件的数据数量的通用方法
     * @param array $where 统计条件数组
     * @return int 符合条件的数据数量
     */
    public function total(array $where)
    {
        return $this->where($where)->count();
    }

    /**
     * 获取多条数据
     * @param array $where 查询条件
     * @param array $whereIn 查询In条件
     * @param mixed $fields 需要获取的字段
     * @param string $order 排序
     * @param int $page 分页
     * @param int $pageSize 分页大小
     * @return mixed
     */
    public function getList(array $where = [], array $whereIn = [], string $fields = '*', string $order = '', int $page = 0, int $pageSize = 10)
    {
        $query = $this->where($where)->field($fields);
        if ($whereIn && count($whereIn) > 0) {
            $key = array_key_first($whereIn);
            $value = preg_split('/\s*,\s*/', implode(',', $whereIn[$key]));
            $query->whereIn($key, $value);
        }
        if ($order) {
            $query->order($order);
        }
        if ($page) {
            return $query->paginate([
                'list_rows' => $pageSize,
                'page' => $page,
            ]);
        } else {
            return $query->select();
        }
    }
    
    /**
     * 获取配置的值
     * @param array $where 条件数组，用于指定要获取的配置
     * @return mixed 配置的值，如果配置信息不存在则返回空数组
     */
    public function getConfigValue(array $where)
    {
        return ($config_info = $this->getOne($where)) &&!empty($config_info)? $config_info['value'] : [];
    }

    /**
     * 设置配置信息
     * @param array $where 条件数组，用于指定要操作的记录
     * @param array $data 包含配置数据的数组
     * @return bool|int 操作结果，成功时返回影响的行数或插入的自增主键，失败时返回 false
     */
    public function setConfig(array $where, array $data)
    {
        $info = $this->getOne($where);
        if (empty($info)) {
            $data = array_merge($where, $data);
            $data[ 'create_time' ] = time();
            $res = $this->create($data);
        } else {
            $data[ 'update_time' ] = time();
            $res = $this->where($where)->save($data);
        }
        return $res;
    }

    /**
     * 更新 JSON 字段中的特定键值
     * @param array $where 条件数组，用于指定要更新的记录
     * @param string $key JSON 字段中的键
     * @param string $value 要设置的新值
     * @return bool 是否更新成功
     */
    public function updateJsonValue(array $where, string $key, string $value)
    {
        // 先获取符合条件的记录
        $record = $this->where($where)->find();
        if ($record) {
            // 获取原始的 JSON 数据
            $jsonData = $record->getData('value');
            // 解析 JSON 数据为数组
            $jsonArray = json_decode($jsonData, true);
            // 更新指定键的值
            $jsonArray[$key] = $value;
            // 将更新后的数组转换回 JSON 字符串
            $updatedJson = json_encode($jsonArray);
            // 更新数据库中的记录
            $record->value = $updatedJson;
            return $record->save();
        }
        return false;
    }
}